import 'dart:async';
import 'package:stream_flutter/models/image_model.dart';
import 'package:stream_flutter/service/list_image.dart';

class Bloc {
  final _controller = StreamController<ImageModel>();

  Stream<ImageModel> get stream => _controller.stream;
  var count = 0;

  Bloc() {
    List<ImageModel> list_image = [];
    Timer.periodic(Duration(seconds: 5), (timer) async {
      list_image = await getImagefromApi() as List<ImageModel>;
    });

    Timer.periodic(Duration(seconds: 5), (timer) {
      //add count to stream
      _controller.sink.add(list_image[count]);
      count++;
      if (count >= list_image.length) {
        dispose();
      }
    });
  }
  dispose() {
    _controller.close();
  }
}

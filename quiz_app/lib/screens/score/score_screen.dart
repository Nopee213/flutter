import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:quiz_app/constants.dart';
import 'package:quiz_app/controllers/question_controller.dart';

class ScoreScreen extends StatelessWidget {
  QuestionController _Qncontroller = Get.put(QuestionController());
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Stack(
      fit: StackFit.expand,
      children: [
        SvgPicture.asset('assets/icons/backg.svg', fit: BoxFit.fill),
        Column(children: [
          Spacer(
            flex: 3,
          ),
          Text(
            "Score",
            style: Theme.of(context)
                .textTheme
                .headline3!
                .copyWith(color: kSecondaryColor),
          ),
          Spacer(),
          Text(
            "${_Qncontroller.correctAns * 10}/${_Qncontroller.questions.length * 10}",
            style: Theme.of(context)
                .textTheme
                .headline4!
                .copyWith(color: kSecondaryColor),
          ),
          Spacer(
            flex: 3,
          ),
        ])
      ],
    ));
  }
}

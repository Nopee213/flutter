import 'package:flutter/material.dart';
import 'package:lab1/Image_Page.dart';
import 'package:lab1/image_model.dart';
import 'List_Image.dart';

void main() async {
  List<ImageModel> lists = [];

  await getImagefromApi().then((value) {
    lists = value.toList();
  });
  //var deck = Deck();
  //print(deck.cards);
  runApp(MyApp(lists));
}

class Deck {
  List<Card> cards = List<Card>.empty(growable: true);

  Deck() {
    var ranks = ['Ace', 'Two', 'Three', 'Four', 'Five'];
    var suits = ['Diamonds', 'Hearts', 'Clubs', 'Spades'];

    for (var rank in ranks) {
      for (var suit in suits) {
        cards.add(new Card(rank: rank, suit: suit));
      }
    }
  }

  String toString() {
    return cards.toString();
  }
}

class Card {
  String suit;
  String rank;

  Card({required this.suit, required this.rank});

  String toString() {
    return '$rank of $suit';
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  List<ImageModel> list;
  MyApp(this.list);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab01',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: App(this.list),
    );
  }
}

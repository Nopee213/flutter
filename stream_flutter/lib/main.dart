import 'package:flutter/material.dart';
import 'package:stream_flutter/bloc/stream_message.dart';
import 'package:stream_flutter/models/image_model.dart';
import 'package:transparent_image/transparent_image.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: MyApp_Stream(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyApp_Stream extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Listmessage();
  }
}

class _Listmessage extends State<MyApp_Stream> {
  Bloc message = Bloc();
  ScrollController scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        print('At the end');
      }
    });
  }

  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<ImageModel> items = [];
    return Scaffold(
        appBar: AppBar(title: Text('Stream app')),
        body: Center(
          child: StreamBuilder(
              stream: Bloc().stream,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return CircularProgressIndicator();
                } else if (snapshot.connectionState == ConnectionState.done) {
                  return Text('done');
                } else if (snapshot.hasError) {
                  return Text('Error!');
                } else {
                  items.add(snapshot.data as ImageModel);
                  return ListView.builder(
                    controller: scrollController,
                    itemBuilder: (context, index) {
                      return Center(
                        child: FadeInImage.assetNetwork(
                          placeholder: 'assets/images/Fidget-spinner.gif',
                          image: items[index].url.toString(),
                        ),
                      );
                    },
                    itemCount: items.length,
                  );
                }
              }),
        ));
  }
}

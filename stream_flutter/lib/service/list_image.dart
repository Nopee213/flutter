import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:stream_flutter/models/image_model.dart';

Future<List<ImageModel>> getImagefromApi() async {
  final url = "https://jsonplaceholder.typicode.com/photos?_start=1&_limit=20";
  final http.Client httpClient = http.Client();
  try {
    final response = await httpClient.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final respondata = json.decode(response.body) as List;
      final List<ImageModel> Images =
          respondata.map((e) => ImageModel(e['id'], e['url'])).toList();
      return Images;
    } else {
      return [];
    }
  } catch (exception) {
    return [];
  }
}

import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:lab1/image_model.dart';
import 'package:lab1/List_Image.dart';

class App extends StatelessWidget {
  List<ImageModel> list;
  App(this.list);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Lab01")),
        body: Container(
            child: ListView.builder(
                itemCount: list.length,
                itemBuilder: (context, index) {
                  return Container(
                    child: Image.network(list[index].url.toString()),
                  );
                })));
  }
}

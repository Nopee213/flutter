import 'dart:async';

class Bloc {
  final _controller = StreamController<String>();

  Stream<String> get stream => _controller.stream;
  var count = 0;
  Bloc() {
    final List<String> list_message = [
      'Facebook',
      'Twiter',
      'Zalo',
      'Instagram',
      'Wechat',
      'F8'
    ];
    Timer.periodic(Duration(seconds: 5), (timer) {
      //add count to stream
      _controller.sink.add(list_message[count]);
      count++;
      if (count >= list_message.length) {
        dispose();
      }
    });
  }
  dispose() {
    _controller.close();
  }
}

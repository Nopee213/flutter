import 'package:flutter/material.dart';
import 'dart:async';

void main() async {
  Bloc a = Bloc();
  int count = 0;
  List<String> b = [];
  b.add('Hello');
  b.add('Stream');
  b.add('1');
  b.add('2');
  b.add('3');
  b.add('4');
  b.add('5');
  for (int i = 0; i < b.length; i++) {
    await a.receiveData(b[i]);
    count++;
  }
  runApp(MyApp(count: count));
}

class Bloc {
  final demoController = StreamController<String>();
  // Retrieve dat
  receiveData(String msg) {
    demoController.sink.add(msg);
  }

  // Listen
  Bloc() {
    // Capture data and processing...
    demoController.stream.listen((message) {
      print(message);
    });
  }
}

class MyApp extends StatelessWidget {
  int count;
  MyApp({required this.count});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    print('haha');
    return MaterialApp(
      title: 'Stream Demo',
      home: Scaffold(
          appBar: AppBar(title: Text('Stream app')),
          body: Center(
            child: Text(
              '$count',
              style: TextStyle(
                  fontStyle: FontStyle.italic,
                  color: Colors.black.withOpacity(0.6),
                  fontSize: 50),
            ),
          )),
    );
  }
}
